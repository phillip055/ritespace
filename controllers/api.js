const {
  promisify
} = require('util');
const request = require('request');
const cheerio = require('cheerio');

var config = {
  'secrets': {
    'clientId': process.env.FOURSQUARE_ID,
    'clientSecret': process.env.FOURSQUARE_SECRET,
    'redirectUrl': process.env.FOURSQUARE_REDIRECT_URL
  }
}

const {
  Venues
} = require('node-foursquare')(config);

const Space = require('../models/Space');


/**
 * GET /login
 * Login Page
 */

exports.getLogin = (req, res) => {
  if (req.user) {
    return res.redirect('/');
  }
  res.render('account/login', {
    title: 'Login'
  });
};

exports.getSpaceForm = (req, res) => {
  console.log(req.user.latestToken)
  Venues.getManaged(req.user.latestToken, (err, _venues) => {
    const userManagedVenues = [{
      id: 'testId',
      name: 'testVenue'
    }]
    if (_venues) {
      userManagedVenues.concat(_venues.venues.items)
    }
    res.render('add-space', {
      title: 'Add space',
      userManagedVenues
    })
  })
}

exports.addSpace = (req, res) => {
  console.log(req.body)
  if (req.body.vid == 'testId') {
    const newSpace = Space()
    newSpace.vid = req.body.vid
    newSpace.name = 'testName'
    newSpace.description = 'testDescription'
    newSpace.latitude = 13
    newSpace.longitude = 100
    newSpace.formattedAddress = 'testFormattedAdress'
    newSpace.rentCost = req.body.price
    newSpace.area = req.body.area
    newSpace.totalCheckIns = 1000
    newSpace.genderDistribution = [{
      gender: 'male',
      checkins: 400
    }, {
      gender: 'female',
      checkins: 600
    }]
    newSpace.ageDistribution = [{
      age: 15,
      checkins: 10
    }, {
      age: 30,
      checkins: 10
    }]
    newSpace.hourDistribution = [{
      hour: '13',
      checkins: 50
    }, {
      hour: '16',
      checkins: 300
    }]
    newSpace.save((dbErr) => {
      res.redirect('/')
    })
  } else {
    Venues.getVenue(req.body.vid, req.user.latestToken, (getInfoErr, getInfoRes) => {
      Venues.getStats(req.body.vid, req.user.latestToken, (err, result) => {
        const newSpace = Space()
        newSpace.vid = req.body.vid
        newSpace.name = getInfoRes.name
        newSpace.description = getInfoRes.description
        newSpace.formattedAddress = getInfoRes.location.formattedAddress
        newSpace.latitude = getInfoRes.location.lat
        newSpace.longitude = getInfoRes.location.lng
        newSpace.rentCost = req.body.price
        newSpace.area = req.body.area
        newSpace.totalCheckIns = result.totalCheckins
        newSpace.genderDistribution = result.genderBreakdown
        newSpace.ageDistribution = res.ageBreakdown
        newSpace.hourDistribution = res.hourBreakdown
        newSpace.save((dbErr) => {
          res.redirect('/')
        })
      })
    })
  }
}

// exports.getSpaces = (req, res) => {
//   return res;
// }

exports.getSpace = (req, res) => {
  Space.findOne({
    'vid': req.params.id
  }, (dbErr, dbRes) => {
    console.log(dbRes)
    res.render('space', {
      space: dbRes
    })
  })
}