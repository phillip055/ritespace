const Space = require('../models/Space');
/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  Space.find().lean().exec((err, result) => {
    console.log(result)
    var markers = []
    result.forEach(element => {
      markers.push({
        'vid': element.vid,
        'name': element.name,
        'latitude': element.latitude,
        'longitude': element.longitude
      })
    });
    res.render('home', {
      title: 'Home',
      markers,
      google_map_api_key: process.env.GOOGLE_MAP_API_KEY,
    });
  });
};