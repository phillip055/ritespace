const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  firstName: { type: String },
  lastName: { type: String},
  gender: {type: String},
  fqID: {type: String, unique: true},
  latestToken: String,
  photoUrl: {type: String}
}, { timestamps: true });

const User = mongoose.model('User', userSchema);

module.exports = User;
