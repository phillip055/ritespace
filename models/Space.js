const mongoose = require('mongoose');

const spaceSchema = new mongoose.Schema({
    vid: {
        type: String,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    formattedAddress: {
        type: String
    },
    rentCost: {
        type: Number
    },
    area: {
        type: Number
    },
    totalCheckIns: {
        type: Number
    },
    genderDistribution: {
        type: Array
    },
    ageDistribution: {
        type: Array
    },
    hourDistribution: {
        type: Array
    }
}, {
    timestamps: true
});

const Space = mongoose.model('Space', spaceSchema);

module.exports = Space;