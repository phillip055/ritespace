const passport = require('passport');
const request = require('request');
const {
  OAuth2Strategy
} = require('passport-oauth');
const {
  Venues,
  Users
} = require('node-foursquare')({
  secrets: {
    clientId: process.env.FOURSQUARE_ID,
    clientSecret: process.env.FOURSQUARE_SECRET,
    redirectUrl: process.env.FOURSQUARE_REDIRECT_URL
  },
  foursquare: {
    mode: 'foursquare',
    version: 20140806,
  }
});
const User = require('../models/User');

passport.serializeUser((user, done) => {
  done(null, user.fqID);
});

passport.deserializeUser((id, done) => {
  User.findOne({
    "fqID": id
  }, (dbErr, user) => {
    done(dbErr, user);
  });
});

/**
 * Foursquare API OAuth.
 */
passport.use('foursquare', new OAuth2Strategy({
    authorizationURL: 'https://foursquare.com/oauth2/authorize',
    tokenURL: 'https://foursquare.com/oauth2/access_token',
    clientID: process.env.FOURSQUARE_ID,
    clientSecret: process.env.FOURSQUARE_SECRET,
    callbackURL: process.env.FOURSQUARE_REDIRECT_URL,
    passReqToCallback: true
  },
  (req, accessToken, refreshToken, profile, done) => {
    Users.getUser('self', accessToken, (err, res) => {
      User.findOne({
        "fqID": res.user.id
      }, (dbErr, user) => {
        if (user != null) {
          user.latestToken = accessToken;
          user.save((err) => {
            done(err, user);
          })
        } else {
          const user = new User();
          user.email = res.user.contact.email;
          user.facebook = res.user.id;
          user.lastToken = accessToken;
          user.firstName = res.user.firstName;
          user.lastName = res.user.lastName;
          user.fqID = res.user.id;
          user.photoUrl = res.user.photo.prefix + res.user.photo.suffix.substring(1);
          user.save((dbErr) => {
            done(dbErr, user);
          });
        }
      });
    });
  }));

/**
 * Login Required middleware.
 */
exports.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
};

/**
 * Authorization Required middleware.
 */
exports.isAuthorized = (req, res, next) => {
  if (req.user.latestToken) {
    next();
  } else {
    res.redirect('/login');
  }
};