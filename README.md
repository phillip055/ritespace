# SpaceRite

## Find the right space for your retail shop

### What is SpaceRite?

SpaceRite tries to help retail shop finding the right space which matches their target audience and space owners to list their space online.

### How does it work?

#### Space Owners

Firstly, they login with their foursquare account and click on the 'Add space' tab, which will allow them to list their 'foursquare venue'  with the area able to rent and it's price.

#### Retail Shops

They can just come to the website and see the locations they are interested in and click on the InfoWindow(after a click on the marker). They will see some stats about the place which would help them make their decision of 'where should I open my shop'.

### Running locally

Add .env to root of the project, you can get these variables from FourSquare and Google development console

```
FOURSQUARE_ID={yours}
FOURSQUARE_SECRET={yours}
FOURSQUARE_REDIRECT_URL={yours}
BASE_URl={localhost:3000}
MONGODB_URI={yours}
SESSION_SECRET={yours}
GOOGLE_ID={yours}
GOOGLE_SECRET={yours}
GOOGLE_MAP_API_KEY={yours}
```

Go!

```javascript
npm install & node app
```

**Prototype Issue**

- To be able to list and get information from foursquare you need to 'claim' the venue. Which costs $20 on Foursquare. ~~so sad~~. I tried to claim 'Phillip's home' but after 12+ hours they rejected the claim.. ~~too bad~~. So on 'Add space', there is just one mocked property(which should not so because it's already added)

**Things to improve**

- Have some nice charts of 'space detail page' using d3
- Form Validation when adding space
- More information about the space
- Some filters on the home page to make decision making easier eg. Price filter, Space Area filter, City Filter
- Clean up code(Always)